﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 

namespace ConsoleApp1
{
    class chromosome
    {
        private int speed; // (kt)
        private int initial_speed;//(kt)
        private int conflict; // the number of conflict
        private int route; // selecting offset route
        private int rta; // time of required time arrival
        private double chr_fitness; // fitness for chromosome
        private int position_x;
        private int position_y;
        private int start;
        private int time;
        private int size;
        Random rand;

        public int Speed { get => speed; set => speed = value; }
        public int Conflict { get => conflict; set => conflict = value; }
        public int Route { get => route; set => route = value; }
        public int Rta { get => rta; set => rta = value; }
        public double Chr_fitness { get => chr_fitness; set => chr_fitness = value; }
        public int Position_x { get => position_x; set => position_x = value; }
        public int Position_y { get => position_y; set => position_y = value; }
        public int Start { get => start; set => start = value; }
        public int Time { get => time; set => time = value; }
        public int Size { get => size; set => size = value; }
        public int Initial_speed { get => initial_speed; set => initial_speed = value; }

        public chromosome(int num , Config conf)//constructor
        {
            this.rand = new Random();
            this.rand = conf.rand;
            this.Speed = new int();
            this.Initial_speed = new int();
            this.Conflict = new int();
            this.Route = new int();
            this.Rta = new int();
            this.Chr_fitness = new double();
            this.Position_x = new int();
            this.Position_y = new int();
            this.Time = new int();
            this.Size = new int();
            this.Size = Config.Size[num];
            this.Initial_speed = Config.Speed[num];//chromsome needs Config to use Random function 
            double speed_rand = (1 + ((this.rand.NextDouble() * 25) - 20) / 100);// upper limit 5% lower limit -20%;
            this.Speed = Config.Speed[num]  ;
            this.Time = Config.Time[num];
            this.Start = Config.Start[num];
            while (true)
            {
                this.Speed = (int)((double)this.Initial_speed * speed_rand);
                if (this.Speed <= 280 && this.Speed >= 250)//max speed and minimum speed
                {
                    break;
                }
                speed_rand = (1 + ((this.rand.NextDouble() * 25) - 20)/100);// upper limit 5% lower limit -20%
            }
        }
        public chromosome(chromosome chr)//copy constructor
        {
            this.rand = new Random();
            this.rand = chr.rand;
            this.Speed = new int();
            this.Initial_speed = new int();
            this.Conflict = new int();
            this.Route = new int();
            this.Rta = new int();
            this.Chr_fitness = new double();
            this.Position_x = new int();
            this.Position_y = new int();
            this.Time = new int();
            this.Size = new int();
            this.Initial_speed = chr.Initial_speed;
            this.Size = chr.Size;
            this.Speed = chr.Speed;
            this.Conflict = chr.Conflict;
            this.Route = chr.Route;
            this.Rta = chr.Rta;
            this.Start = chr.Start;
            this.Chr_fitness = chr.Chr_fitness;
            this.Time = chr.Time;
            this.Position_x = chr.Position_x;
            this.Position_y = chr.Position_y;
        }
    }
}
