﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        private int seed;
        public int Seed
        {
            get { return seed; }
            set { seed = value; }
        }
        public Program(int i)
        {
            seed = i;

        }
        static void Main(string[] args)
        {
            for (int m = 0; m < 1; m++)
            {
                Program program = new Program(m);
                Field field = new Field();
                Config config = new Config(program.Seed);
                system_operation op = new system_operation(config,field, program.Seed);
                //double[] fitbox = new double[(int)(Config.Generation)];
                op.generation();              
                //op.print();
            }

            ConsoleKeyInfo start = Console.ReadKey();
            switch (start.Key)
            {
                case ConsoleKey.Enter:
                    Console.WriteLine("終了します");
                    break;
                default:
                    Console.WriteLine("入力エラー");
                    break;
            }
        }
    }
}
