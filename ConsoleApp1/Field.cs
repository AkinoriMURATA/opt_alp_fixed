﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Field//definition map
    {
        int node1,node2,node3,node4;

        int link1,link2,link3;

        public Field()
        {
            this.Node1 = new int();
            this.Node2 = new int();
            this.Node3 = new int();
            this.Node4 = new int();
            this.Link1 = new int(); Link1 = 49;//80;//49;//DVL-SOKMU
            this.Link2 = new int(); Link2 = 37;//70;//37;//DPESOKMU
            this.Link3 = new int(); Link3 = 43;//40;//43;//SOKMU-Finish

        }
        public Field(Field field)
        {
            this.Node1 = new int();
            this.Node2 = new int();
            this.Node3 = new int();
            this.Node4 = new int();
            this.Link1 = new int(); Link1 = field.Link1;//DVL-SOKMU
            this.Link2 = new int(); Link2 = field.Link2;//DPESOKMU
            this.Link3 = new int(); Link3 = field.Link3;//SOKMU-Finish

        }
        public int Node1 { get => node1; set => node1 = value; }
        public int Node2 { get => node2; set => node2 = value; }
        public int Node3 { get => node3; set => node3 = value; }
        public int Node4 { get => node4; set => node4 = value; }
        public int Link1 { get => link1; set => link1 = value; }
        public int Link2 { get => link2; set => link2 = value; }
        public int Link3 { get => link3; set => link3 = value; }

        public int distance(int start)
        {
            int ret=0;
            if (start == 1)
            {
                ret = this.Link1 + this.Link3;
            }
            else// start ==2
            {
                ret = this.Link2 + this.Link3;
            }
            return ret;
        }
    }
}
