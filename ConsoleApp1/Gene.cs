﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Gene
    {
        Config conf;
        Field fie;
        Random random;
        //private double fitness;// fitness for individual
        private List<chromosome> gene_list; // one chromosome is aircraft

        internal List<chromosome> Gene_list { get => gene_list; set => gene_list = value; }
        //public double Fitness { get => fitness; set => fitness = value; }

        public void Mutation()// mutation operator
        {
            double rd = conf.rand.NextDouble();
            if (rd < Config.Mutation_rate)
            {
                double rand = this.random.NextDouble();
                int total_conflict = 0;
                int sum_conflict = 0;
                for (int i = 0; i < this.Gene_list.Count; i++)// count total conflict
                {
                    total_conflict = total_conflict + this.gene_list[i].Conflict;
                }
                for (int i = 0; i < this.Gene_list.Count; i++)
                {
                    sum_conflict = sum_conflict + 1;
                    double speed_rand = (1 + ((this.random.NextDouble() * 25) - 20) / 100);// upper limit 5% lower limit -20%
                    if (rand <= (double)((double)sum_conflict / (double)total_conflict))
                    {
                        while (true)
                        {
                            this.Gene_list[i].Speed = (int)((double)this.Gene_list[i].Initial_speed * speed_rand);
                            if (this.Gene_list[i].Speed <= 280 && this.Gene_list[i].Speed >= 250)//max speed and minimum speed
                            {
                                break;
                            }
                            speed_rand = (1 + ((this.random.NextDouble() * 25) - 20) / 100);// upper limit 5% lower limit -20%
                        }
                    }
                    rand = this.random.NextDouble();
                }
            }
            
        }
        public void Localsearch()
        {
            int[] speed_revise = new int[this.Gene_list.Count];
            for (int i = 0; i < this.Gene_list.Count; i++)
            {
                speed_revise[i] = this.Gene_list[i].Speed;
            }
            int[] ord_box = new int[(this.Gene_list.Count)];
            //this.Infeasible = 0;//initialize
            for (int i = 0; i < this.Gene_list.Count; i++)
            {
                this.Gene_list[i].Conflict = 0;// Initialize
            }
            int[] t_box = new int[(this.Gene_list.Count)];
            for (int i = 0; i < this.Gene_list.Count; i++)//link1
            {
                if (this.Gene_list[i].Start == 1)
                {
                    t_box[i] = this.Gene_list[i].Time + (int)((double)fie.Link1 / (double)this.Gene_list[i].Speed * 3600);// time unit conversion                 }

                }
                else
                {
                    //t_box[i] = 0;// not through
                }
            }
            for (int i = 0; i < this.Gene_list.Count; i++)//link2
            {
                if (this.Gene_list[i].Start == 2)
                {
                    t_box[i] = this.Gene_list[i].Time + (int)((double)fie.Link2 / (double)this.Gene_list[i].Speed * 3600);//
                }
                else
                {
                    //t_box[i] = 0;// not through
                }
            }
            int[] t_box_cp = new int[(this.Gene_list.Count)];
            for (int i = 0; i < this.Gene_list.Count; i++)
            {
                t_box_cp[i] = t_box[i];
            }
            Array.Sort(t_box_cp);
            for (int i = 0; i < this.Gene_list.Count; i++)
            {
                for (int j = 0; j < this.Gene_list.Count; j++)
                {
                    if (t_box[i] == t_box_cp[j])
                    {
                        ord_box[i] = j;
                    }
                }
            }
           
            //t_box.RemoveAll(num => num == 0);// remove number #0#
            for (int a = 0; a < t_box.Count(); a++)//first part
            {
                for (int b = a + 1; b < t_box.Count(); b++)
                {
                    if (Math.Abs(t_box[a] - t_box[b]) <= 100)
                    {
                        if (t_box[a] - t_box[b] > 0) //trailing aircraft is a
                        {
                            if (this.Gene_list[b].Size == 2 && this.Gene_list[a].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 80)
                                {
                                    if (this.Gene_list[a].Start == 1)
                                    {
                                        speed_revise[a] = (int)((double)fie.Link1) / (t_box[a] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[a].Time) / 3600;
                                    }
                                    else
                                    {
                                        speed_revise[a] = (int)((double)fie.Link2) / (t_box[a] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[a].Time) / 3600;
                                    }
                                    if (speed_revise[a] > 280 || speed_revise[a] > this.Gene_list[a].Speed * 1.05)
                                    {
                                        speed_revise[a] = (int)Math.Min(280, (double)this.Gene_list[a].Speed * 1.05);
                                    }
                                    else if (speed_revise[a] < 250 || speed_revise[a] > (double)this.Gene_list[a].Speed * 0.8)
                                    {
                                        speed_revise[a] = (int)Math.Max(250, (double)this.Gene_list[a].Speed * 0.8);
                                    }
                                }
                            }
                            else if (this.Gene_list[b].Size == 2 && this.Gene_list[a].Size == 1)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 100)
                                {
                                    if (this.Gene_list[a].Start == 1)
                                    {
                                        speed_revise[a] = (int)((double)fie.Link1) / (t_box[a] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[a].Time) / 3600;
                                    }
                                    else
                                    {
                                        speed_revise[a] = (int)((double)fie.Link2) / (t_box[a] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[a].Time) / 3600;
                                    }
                                    if (speed_revise[a] > 280 || speed_revise[a] > (double)this.Gene_list[a].Speed * 1.05)
                                    {
                                        speed_revise[a] = (int)Math.Min(280, (double)this.Gene_list[a].Speed * 1.05);
                                    }
                                    else if (speed_revise[a] < 250 || speed_revise[a] > (double)this.Gene_list[a].Speed * 0.8)
                                    {
                                        speed_revise[a] = (int)Math.Max(250, (double)this.Gene_list[a].Speed * 0.8);
                                    }
                                }
                            }
                            else if (this.Gene_list[b].Size == 1 && this.Gene_list[a].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 60)
                                {
                                    if (this.Gene_list[a].Start == 1)
                                    {
                                        speed_revise[a] = (int)((double)fie.Link1) / (t_box[a] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[a].Time) / 3600;
                                    }
                                    else
                                    {
                                        speed_revise[a] = (int)((double)fie.Link2) / (t_box[a] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[a].Time) / 3600;
                                    }
                                    if (speed_revise[a] > 280 || speed_revise[a] > (double)this.Gene_list[a].Speed * 1.05)
                                    {
                                        speed_revise[a] = (int)Math.Min(280, (double)this.Gene_list[a].Speed * 1.05);
                                    }
                                    else if (speed_revise[a] < 250 || speed_revise[a] > (double)this.Gene_list[a].Speed * 0.8)
                                    {
                                        speed_revise[a] = (int)Math.Max(250, (double)this.Gene_list[a].Speed * 0.8);
                                    }
                                }
                            }
                            else//(this.Gene_list[b].Size == 1 || this.Gene_list[a].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 60)
                                {
                                    if (this.Gene_list[a].Start == 1)
                                    {
                                        speed_revise[a] = (int)((double)fie.Link1) / (t_box[a] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[a].Time) / 3600;
                                    }
                                    else
                                    {
                                        speed_revise[a] = (int)((double)fie.Link2) / (t_box[a] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[a].Time) / 3600;
                                    }
                                    if (speed_revise[a] > 280 || speed_revise[a] > (double)this.Gene_list[a].Speed * 1.05)
                                    {
                                        speed_revise[a] = (int)Math.Min(280, (double)this.Gene_list[a].Speed * 1.05);
                                    }
                                    else if (speed_revise[a] < 250 || speed_revise[a] > (double)this.Gene_list[a].Speed * 0.8)
                                    {
                                        speed_revise[a] = (int)Math.Max(250, (double)this.Gene_list[a].Speed * 0.8);
                                    }
                                }
                            }
                        }
                        else// trailing b
                        {
                            if (this.Gene_list[a].Size == 2 && this.Gene_list[b].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 80)
                                {
                                    if (this.Gene_list[b].Start == 1)
                                    {
                                        speed_revise[b] = (int)((double)fie.Link1) / (t_box[b] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[b].Time) / 3600;
                                    }
                                    else
                                    {
                                        speed_revise[b] = (int)((double)fie.Link2) / (t_box[b] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[b].Time) / 3600;
                                    }
                                    if (speed_revise[b] > 280 || speed_revise[b] > (double)this.Gene_list[b].Speed * 1.05)
                                    {
                                        speed_revise[b] = (int)Math.Min(280, (double)this.Gene_list[b].Speed * 1.05);
                                    }
                                    else if (speed_revise[b] < 250 || speed_revise[b] > (double)this.Gene_list[b].Speed * 0.8)
                                    {
                                        speed_revise[b] = (int)Math.Max(250, (double)this.Gene_list[b].Speed * 0.8);
                                    }
                                }
                            }
                            else if (this.Gene_list[a].Size == 2 && this.Gene_list[b].Size == 1)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 100)
                                {
                                    if (this.Gene_list[a].Start == 1)
                                    {
                                        speed_revise[b] = (int)((double)fie.Link1) / (t_box[b] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[b].Time) / 3600;
                                    }
                                    else
                                    {
                                        speed_revise[b] = (int)((double)fie.Link2) / (t_box[b] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[b].Time) / 3600;
                                    }
                                    if (speed_revise[b] > 280 || speed_revise[b] > (double)this.Gene_list[b].Speed * 1.05)
                                    {
                                        speed_revise[b] = (int)Math.Min(280, (double)this.Gene_list[b].Speed * 1.05);
                                    }
                                    else if (speed_revise[b] < 250 || speed_revise[b] > (double)this.Gene_list[b].Speed * 0.8)
                                    {
                                        speed_revise[b] = (int)Math.Max(250, (double)this.Gene_list[b].Speed * 0.8);
                                    }
                                }
                            }
                            else if (this.Gene_list[a].Size == 1 && this.Gene_list[b].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 60)
                                {
                                    if (this.Gene_list[b].Start == 1)
                                    {
                                        speed_revise[b] = (int)((double)fie.Link1) / (t_box[b] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[b].Time) / 3600;
                                    }
                                    else
                                    {
                                        speed_revise[b] = (int)((double)fie.Link2) / (t_box[b] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[b].Time) / 3600;
                                    }
                                    if (speed_revise[b] > 280 || speed_revise[b] > (double)this.Gene_list[b].Speed * 1.05)
                                    {
                                        speed_revise[b] = (int)Math.Min(280, (double)this.Gene_list[b].Speed * 1.05);
                                    }
                                    else if (speed_revise[b] < 250 || speed_revise[b] > (double)this.Gene_list[b].Speed * 0.8)
                                    {
                                        speed_revise[b] = (int)Math.Max(250, (double)this.Gene_list[b].Speed * 0.8);
                                    }
                                }
                            }
                            else//(this.Gene_list[b].Size == 1 || this.Gene_list[a].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 60)
                                {
                                    if (this.Gene_list[b].Start == 1)
                                    {
                                        speed_revise[b] = (int)((double)fie.Link1) / (t_box[b] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[b].Time) / 3600;
                                    }
                                    else
                                    {
                                        speed_revise[b] = (int)((double)fie.Link2) / (t_box[b] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[b].Time) / 3600;
                                    }
                                    if (speed_revise[b] > 280 || speed_revise[b] > (double)this.Gene_list[b].Speed * 1.05)
                                    {
                                        speed_revise[b] = (int)Math.Min(280, (double)this.Gene_list[b].Speed * 1.05);
                                    }
                                    else if (speed_revise[b] < 250 || speed_revise[b] > (double)this.Gene_list[b].Speed * 0.8)
                                    {
                                        speed_revise[b] = (int)Math.Max(250, (double)this.Gene_list[b].Speed * 0.8);
                                    }
                                }
                            }
                        }
                    }
                }
            }// from here first part 
            for (int i = 0; i < this.Gene_list.Count; i++)
            {
                t_box[i] = t_box[i] + (int)((double)fie.Link3 / (double)this.Gene_list[i].Speed * 3600);//sum to GOAL
            }
            for (int a = 0; a < t_box.Count(); a++)
            {
                for (int b = a + 1; b < t_box.Count(); b++)
                {
                    if (Math.Abs(t_box[a] - t_box[b]) < 100)
                    {
                        if (t_box[a] - t_box[b] > 0) //trailing aircraft is a
                        {
                            if (this.Gene_list[b].Size == 2 && this.Gene_list[a].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 80 || (ord_box[a] < ord_box[b]))
                                {
                                    if (this.Gene_list[a].Start == 1)
                                    {
                                        speed_revise[a] = (int)(((double)fie.Link1 + (double)fie.Link3)) / (t_box[a] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[a].Time) / 3600;
                                    }
                                    else
                                    {
                                        speed_revise[a] = (int)(((double)fie.Link2 + (double)fie.Link3)) / (t_box[a] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[a].Time) / 3600;
                                    }
                                    if (speed_revise[a] > 280 || speed_revise[a] > (double)this.Gene_list[a].Speed * 1.05)
                                    {
                                        speed_revise[a] = (int)Math.Min(280, (double)this.Gene_list[a].Speed * 1.05);
                                    }
                                    else if (speed_revise[a] < 250 || speed_revise[a] > (double)this.Gene_list[a].Speed * 0.8)
                                    {
                                        speed_revise[a] = (int)Math.Max(250, (double)this.Gene_list[a].Speed * 0.8);
                                    }
                                }
                            }
                            else if (this.Gene_list[b].Size == 2 && this.Gene_list[a].Size == 1)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 100 || (ord_box[a] < ord_box[b]))
                                {
                                    if (this.Gene_list[a].Start == 1)
                                    {
                                        speed_revise[a] = (int)(((double)fie.Link1 + (double)fie.Link3)) / (t_box[a] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[a].Time) / 3600;
                                    }
                                    else
                                    {
                                        speed_revise[a] = (int)(((double)fie.Link2 + (double)fie.Link3)) / (t_box[a] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[a].Time) / 3600;
                                    }
                                    if (speed_revise[a] > 280 || speed_revise[a] > (double)this.Gene_list[a].Speed * 1.05)
                                    {
                                        speed_revise[a] = (int)Math.Min(280, (double)this.Gene_list[a].Speed * 1.05);
                                    }
                                    else if (speed_revise[a] < 250 || speed_revise[a] > (double)this.Gene_list[a].Speed * 0.8)
                                    {
                                        speed_revise[a] = (int)Math.Max(250, (double)this.Gene_list[a].Speed * 0.8);
                                    }
                                }
                            }
                            else if (this.Gene_list[b].Size == 1 && this.Gene_list[a].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 60 || (ord_box[a] < ord_box[b]))
                                {
                                    if (this.Gene_list[a].Start == 1)
                                    {
                                        speed_revise[a] = (int)(((double)fie.Link1 + (double)fie.Link3)) / (t_box[a] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[a].Time) / 3600;
                                    }
                                    else
                                    {
                                        speed_revise[a] = (int)(((double)fie.Link2 + (double)fie.Link3)) / (t_box[a] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[a].Time) / 3600;
                                    }
                                    if (speed_revise[a] > 280 || speed_revise[a] > (double)this.Gene_list[a].Speed * 1.05)
                                    {
                                        speed_revise[a] = (int)Math.Min(280, (double)this.Gene_list[a].Speed * 1.05);
                                    }
                                    else if (speed_revise[a] < 250 || speed_revise[a] > (double)this.Gene_list[a].Speed * 0.8)
                                    {
                                        speed_revise[a] = (int)Math.Max(250, (double)this.Gene_list[a].Speed * 0.8);
                                    }
                                }
                            }
                            else//(this.Gene_list[b].Size == 1 || this.Gene_list[a].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 60 || (ord_box[a] < ord_box[b]))
                                {
                                    if (this.Gene_list[a].Start == 1)
                                    {
                                        speed_revise[a] = (int)(((double)fie.Link1 + (double)fie.Link3)) / (t_box[a] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[a].Time) / 3600;
                                    }
                                    else
                                    {
                                        speed_revise[a] = (int)(((double)fie.Link2 + (double)fie.Link3)) / (t_box[a] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[a].Time) / 3600;
                                    }
                                    if (speed_revise[a] > 280 || speed_revise[a] > (double)this.Gene_list[a].Speed * 1.05)
                                    {
                                        speed_revise[a] = (int)Math.Min(280, (double)this.Gene_list[a].Speed * 1.05);
                                    }
                                    else if (speed_revise[a] < 250 || speed_revise[a] > (double)this.Gene_list[a].Speed * 0.8)
                                    {
                                        speed_revise[a] = (int)Math.Max(250, (double)this.Gene_list[a].Speed * 0.8);
                                    }
                                }
                            }
                        }
                        else// trailing b
                        {
                            if (this.Gene_list[a].Size == 2 && this.Gene_list[b].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 80 || (ord_box[b] < ord_box[a]))
                                {
                                    if (this.Gene_list[b].Start == 1)
                                    {
                                        speed_revise[b] = (int)((double)fie.Link1 + (double)fie.Link3) / (t_box[b] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[b].Time) / 3600;
                                    }
                                    else
                                    {
                                        speed_revise[b] = (int)((double)fie.Link2 + (double)fie.Link3) / (t_box[b] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[b].Time) / 3600;
                                    }
                                    if (speed_revise[b] > 280 || speed_revise[b] > (double)this.Gene_list[b].Speed * 1.05)
                                    {
                                        speed_revise[b] = (int)Math.Min(280, (double)this.Gene_list[b].Speed * 1.05);
                                    }
                                    else if (speed_revise[b] < 250 || speed_revise[b] > (double)this.Gene_list[b].Speed * 0.8)
                                    {
                                        speed_revise[b] = (int)Math.Max(250, (double)this.Gene_list[b].Speed * 0.8);
                                    }
                                }
                            }
                            else if (this.Gene_list[a].Size == 2 && this.Gene_list[b].Size == 1)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 100 || (ord_box[b] < ord_box[a]))
                                {
                                    if (this.Gene_list[b].Start == 1)
                                    {
                                        speed_revise[b] = (int)((double)fie.Link1 + (double)fie.Link3) / (t_box[b] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[b].Time) / 3600;
                                    }
                                    else
                                    {
                                        speed_revise[b] = (int)((double)fie.Link2 + (double)fie.Link3) / (t_box[b] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[b].Time) / 3600;
                                    }
                                    if (speed_revise[b] > 280 || speed_revise[b] > (double)this.Gene_list[b].Speed * 1.05)
                                    {
                                        speed_revise[b] = (int)Math.Min(280, (double)this.Gene_list[b].Speed * 1.05);
                                    }
                                    else if (speed_revise[b] < 250 || speed_revise[b] > (double)this.Gene_list[b].Speed * 0.8)
                                    {
                                        speed_revise[b] = (int)Math.Max(250, (double)this.Gene_list[b].Speed * 0.8);
                                    }
                                }
                            }
                            else if (this.Gene_list[a].Size == 1 && this.Gene_list[b].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 60 || (ord_box[b] < ord_box[a]))
                                {
                                    if (this.Gene_list[b].Start == 1)
                                    {
                                        speed_revise[b] = (int)((double)fie.Link1 + (double)fie.Link3) / (t_box[b] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[b].Time) / 3600;
                                    }
                                    else
                                    {
                                        speed_revise[b] = (int)((double)fie.Link2 + (double)fie.Link3) / (t_box[b] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[b].Time) / 3600;
                                    }
                                    if (speed_revise[b] > 280 || speed_revise[b] > (double)this.Gene_list[b].Speed * 1.05)
                                    {
                                        speed_revise[b] = (int)Math.Min(280, (double)this.Gene_list[b].Speed * 1.05);
                                    }
                                    else if (speed_revise[b] < 250 || speed_revise[b] > (double)this.Gene_list[b].Speed * 0.8)
                                    {
                                        speed_revise[b] = (int)Math.Max(250, (double)this.Gene_list[b].Speed * 0.8);
                                    }
                                }
                            }
                            else//(this.Gene_list[b].Size == 1 || this.Gene_list[a].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 60 || (ord_box[b] < ord_box[a]))
                                {
                                    if (this.Gene_list[b].Start == 1)
                                    {
                                        speed_revise[b] = (int)((double)fie.Link1 + (double)fie.Link3) / (t_box[b] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[b].Time) / 3600;
                                    }
                                    else
                                    {
                                        speed_revise[b] = (int)((double)fie.Link2 + (double)fie.Link3) / (t_box[b] + Math.Abs(t_box[a] - t_box[b]) - this.Gene_list[b].Time) / 3600;
                                    }
                                    if (speed_revise[b] > 280 || speed_revise[b] > (double)this.Gene_list[b].Speed * 1.05)
                                    {
                                        speed_revise[b] = (int)Math.Min(280, (double)this.Gene_list[b].Speed * 1.05);
                                    }
                                    else if (speed_revise[b] < 250 || speed_revise[b] > (double)this.Gene_list[b].Speed * 0.8)
                                    {
                                        speed_revise[b] = (int)Math.Max(250, (double)this.Gene_list[b].Speed * 0.8);
                                    }
                                }
                            }
                        }
                    }
                }
                
            }
            for (int i = 0; i < speed_revise.Count(); i++)
            {
                this.Gene_list[i].Speed = speed_revise[i];
            }
        }
        public Gene(Config config, Field field)//constructor
        {
            this.fie = new Field(field);
            this.conf = new Config(config);
            this.random = new Random();
            this.random = config.rand;
            //this.Fitness = new double();
            this.Gene_list = new List<chromosome>();
            for (int i = 0; i < Config.Total_aircraft; i++)
            {
                this.Gene_list.Add(new chromosome(i,conf)); 
            }
        }
        public Gene(Gene g)
        {
            this.fie = new Field(g.fie);
            this.conf = new Config(g.conf);
            this.random = new Random();
            this.random = g.random;
            //this.Fitness = new double();
            //this.Fitness = g.Fitness;
            this.Gene_list = new List<chromosome>();
            for (int i = 0; i < g.Gene_list.Count; i++)
            {
                this.Gene_list.Add(new chromosome(g.Gene_list[i]));
            }
        }
        public void Print_gene()
        {
            int a = this.Gene_list.Count;
            for (int i = 0; i < a; i++)
            {
                Console.WriteLine("gene[" + i + "]:Speed:"+","+ this.Gene_list[i].Speed +",Time:,"+ this.Gene_list[i].Time + ",Size:," + this.Gene_list[i].Size+",Start:,"+ this.Gene_list[i].Start);
            }
        }
    }
}
