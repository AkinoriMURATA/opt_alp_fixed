﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp1
{
    class system_operation
    {
        Config con;
        Field fie;
        Population population;
        public system_operation(Config config, Field field, int seed)//constructor
        {
            this.con = new Config(seed);
            this.fie = new Field();
            this.population = new Population(con,fie);
            this.population.Pop = this.population.Pop.OrderBy(n => n.Infeasible).ThenBy(n => n.Fitness_ind).ToArray();
        }
        public void generation()
        {
            double[] fitbox = new double[(int)(Config.Generation)];
            for (int i = 0; i < Config.Generation; i++)
            {
            this.population.crossOver();
            this.population.Mutation();
            this.population.Pop = this.population.Pop.OrderBy(n => n.Infeasible).ThenBy(n => n.Fitness_ind).ToArray();

                //for (int i = 0; i < this.population.Pop.Count(); i++)
                //{
                //    this.population.Pop[i].print_ind();
                //}
                //for (int j = 0; j < 1; j++)
                //{
                //    this.population.Pop[j].Gene.Print_gene();
                //}
                fitbox[i] = this.population.Pop[0].Fitness_ind;
            }
            //for (int i = 0; i < fitbox.Count(); i=i+10)
            //{
            //    Console.WriteLine(fitbox[i]);
            //}
            for (int j = 0; j < 1; j++)
            {
                this.population.Pop[j].Gene.Print_gene();
            }
            this.csv(fitbox);

        }
        public void print()
        {
            for (int i = 0; i < this.population.Pop.Count(); i++)
            {
                this.population.Pop[i].print_ind();
            }
            for (int i = 0; i < 1; i++)
            {
                this.population.Pop[i].Gene.Print_gene();
            }
        }
        public void csv(double[] box)
        {
            string fileName = "Finess_geration.csv";
            using (StreamWriter sw = new StreamWriter(fileName))
            {
                for (int i = 0; i < box.Count(); i++)
                {
                    sw.WriteLine(i + "," + "Fitness," + box[i]);
                }
            }
        }


    }
}
