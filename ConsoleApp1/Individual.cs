﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Individual
    {
        private Gene gene;
        private double fitness_ind;
        private int infeasible;
        Field fie;
        internal Gene Gene { get => gene; set => gene = value; }
        public double Fitness_ind { get => fitness_ind; set => fitness_ind = value; }
        public int Infeasible { get => infeasible; set => infeasible = value; }

        public void ind_fcalc(Field field)// calculation of fitness
        {
            this.Fitness_ind = 0;
            for (int i = 0; i < this.Gene.Gene_list.Count; i++)
            {
                this.Fitness_ind = this.Fitness_ind + this.Gene.Gene_list[i].Time;
            }
            for (int i = 0; i < this.Gene.Gene_list.Count; i++)
            {
                int distance = field.distance(this.Gene.Gene_list[i].Start);
                this.Fitness_ind= this.Fitness_ind + (double)((double)distance / (double)this.Gene.Gene_list[i].Speed)*3600;// total distance
                this.conflict(field);
            }
        }
        public void conflict(Field field)
        {
            int[] ord_box = new int[(this.Gene.Gene_list.Count)];
            this.Infeasible = 0;//initialize
            for (int i = 0; i < this.Gene.Gene_list.Count; i++)
            {
                this.Gene.Gene_list[i].Conflict = 0;// Initialize
            }
            int[] t_box= new int[(this.Gene.Gene_list.Count)];
            for (int i = 0; i < this.Gene.Gene_list.Count; i++)//link1
            {
                if (this.Gene.Gene_list[i].Start == 1)
                {
                    t_box[i] = this.Gene.Gene_list[i].Time + (int)((double)field.Link1 / (double)this.Gene.Gene_list[i].Speed * 3600);// time unit conversion                 }

                }
                else
                {
                    //t_box[i] = 0;// not through
                }                
            }
            for (int i = 0; i < this.Gene.Gene_list.Count; i++)//link2
            {
                if (this.Gene.Gene_list[i].Start == 2)
                {
                    t_box[i] = this.Gene.Gene_list[i].Time + (int)((double)field.Link2 / (double)this.Gene.Gene_list[i].Speed * 3600);//
                }
                else
                {
                    //t_box[i] = 0;// not through
                }
            }
            int[] t_box_cp = new int[(this.Gene.Gene_list.Count)];
            for (int i = 0; i < this.Gene.Gene_list.Count; i++)
            {
                t_box_cp[i] = t_box[i];
            }
            Array.Sort(t_box_cp);
            for (int i = 0; i < this.Gene.Gene_list.Count; i++)
            {
                for (int j = 0; j < this.Gene.Gene_list.Count; j++)
                {
                    if (t_box[i]== t_box_cp[j])
                    {
                        ord_box[i] = j;
                    }
                }                
            }
            //t_box.RemoveAll(num => num == 0);// remove number #0#
            for (int a = 0; a < t_box.Count(); a++)//first part
            { 
                for (int b = a + 1; b < t_box.Count(); b++)
                {
                    if (Math.Abs(t_box[a]-t_box[b])<=100)
                    {
                        if (t_box[a] - t_box[b]>0) //trailing aircraft is a
                        {
                            if (this.Gene.Gene_list[b].Size == 2 && this.Gene.Gene_list[a].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 80 )
                                {
                                    this.infeasible = 1;
                                    this.Gene.Gene_list[a].Conflict=1;
                                }
                            }
                            else if (this.Gene.Gene_list[b].Size == 2 && this.Gene.Gene_list[a].Size == 1)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 100)
                                {
                                    this.infeasible = 1;
                                    this.Gene.Gene_list[a].Conflict = 1;
                                }
                            }
                            else if (this.Gene.Gene_list[b].Size == 1 && this.Gene.Gene_list[a].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 60)
                                {
                                    this.infeasible = 1;
                                    this.Gene.Gene_list[a].Conflict = 1;
                                }
                            }
                            else//(this.Gene.Gene_list[b].Size == 1 || this.Gene.Gene_list[a].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 60)
                                {
                                    this.infeasible = 1;
                                    this.Gene.Gene_list[a].Conflict = 1;
                                }
                            }
                        }
                        else// trailing is b
                        {
                            if (this.Gene.Gene_list[a].Size == 2 && this.Gene.Gene_list[b].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 80)
                                {
                                    this.infeasible = 1;
                                    this.Gene.Gene_list[b].Conflict = 1;
                                }
                            }
                            else if (this.Gene.Gene_list[a].Size == 2 && this.Gene.Gene_list[b].Size == 1)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 100)
                                {
                                    this.infeasible = 1;
                                    this.Gene.Gene_list[b].Conflict = 1;
                                }
                            }
                            else if (this.Gene.Gene_list[a].Size == 1 && this.Gene.Gene_list[b].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 60)
                                {
                                    this.infeasible = 1;
                                    this.Gene.Gene_list[b].Conflict = 1;
                                }
                            }
                            else//(this.Gene.Gene_list[b].Size == 1 || this.Gene.Gene_list[a].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 60)
                                {
                                    this.infeasible = 1;
                                    this.Gene.Gene_list[b].Conflict = 1;
                                }
                            }
                        }
                    }
                }              
            }// from here first part 
            for (int i = 0; i < this.Gene.Gene_list.Count; i++)
            {
                t_box[i] = t_box[i] + (int)((double)field.Link3 / (double)this.Gene.Gene_list[i].Speed * 3600);//sum to GOAL
            }
            for (int a = 0; a < t_box.Count(); a++)
            {
                for (int b = a+1; b < t_box.Count(); b++)
                {
                    if (Math.Abs(t_box[a] - t_box[b]) < 100)
                    {
                        if (t_box[a] - t_box[b] > 0) //trailing aircraft is a
                        {
                            if (this.Gene.Gene_list[b].Size == 2 && this.Gene.Gene_list[a].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) <80 || (ord_box[a] < ord_box[b]))
                                {
                                    this.infeasible = 1;
                                    this.Gene.Gene_list[a].Conflict = 1;
                                }
                            }
                            else if (this.Gene.Gene_list[b].Size == 2 && this.Gene.Gene_list[a].Size == 1)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 100 || (ord_box[a] < ord_box[b]))
                                {
                                    this.infeasible = 1;
                                    this.Gene.Gene_list[a].Conflict = 1;
                                }
                            }
                            else if (this.Gene.Gene_list[b].Size == 1 && this.Gene.Gene_list[a].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 60 || (ord_box[a] < ord_box[b]))
                                {
                                    this.infeasible = 1;
                                    this.Gene.Gene_list[a].Conflict = 1;
                                }
                            }
                            else//(this.Gene.Gene_list[b].Size == 1 || this.Gene.Gene_list[a].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 60 || (ord_box[a] < ord_box[b]))
                                {
                                    this.infeasible = 1;
                                    this.Gene.Gene_list[a].Conflict = 1;
                                }
                            }
                        }
                        else// trailing is b
                        {
                            if (this.Gene.Gene_list[a].Size == 2 && this.Gene.Gene_list[b].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 80 || (ord_box[b] < ord_box[a]))
                                {
                                    this.infeasible = 1;
                                    this.Gene.Gene_list[b].Conflict = 1;
                                }
                            }
                            else if (this.Gene.Gene_list[a].Size == 2 && this.Gene.Gene_list[b].Size == 1)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 100 || (ord_box[b] < ord_box[a]))
                                {
                                    this.infeasible = 1;
                                    this.Gene.Gene_list[b].Conflict = 1;
                                }
                            }
                            else if (this.Gene.Gene_list[a].Size == 1 && this.Gene.Gene_list[b].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 60 || (ord_box[b] < ord_box[a]))
                                {
                                    this.infeasible = 1;
                                    this.Gene.Gene_list[b].Conflict = 1;
                                }
                            }
                            else//(this.Gene.Gene_list[b].Size == 1 || this.Gene.Gene_list[a].Size == 2)
                            {
                                if (Math.Abs(t_box[a] - t_box[b]) < 60 || (ord_box[b] < ord_box[a]))
                                {
                                    this.infeasible = 1;
                                    this.Gene.Gene_list[b].Conflict = 1;
                                }
                            }
                        }
                    }
                }

            }
        }
        public void print_ind()
        {
            Console.WriteLine("Fitness_ind:"+","+this.Fitness_ind+ ",infiesible:," + this.Infeasible);
        }
        public Individual(Config conf, Field field)
        {
            this.fie = new Field(field);
            this.Gene = new Gene(conf,field);
            this.Fitness_ind = new double();
            this.Infeasible = new int();
            this.ind_fcalc(field);
        }
        public Individual(Individual ind)
        {
            this.Fitness_ind = new double();
            this.Fitness_ind = ind.Fitness_ind;
            this.Infeasible = new int();
            this.Infeasible = ind.Infeasible;
            this.Gene=  new Gene(ind.Gene);
        }
    }
}
