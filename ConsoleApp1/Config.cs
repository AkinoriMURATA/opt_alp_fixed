﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Config
    {
        private static int populationsize = 100;
        private static int total_aircraft = 3;// total_number of aircraft
        private static double mutation_rate = 0.3;
        private static double generation = 1000;//000;
        public Random rand;
        public Config(int seed)
        {
            rand = new Random(seed);
        }
        public Config(Config conf)
        {
            rand = new Random();
            rand = conf.rand;
        }
        private static int[] start = { 1, 1, 2 };
        private static int[] size = { 2, 2, 1 };// H->2,M->1
        private static int[] speed ={260, 260, 260};
        private static int[] time= {0,120,230};//140 -> 210 change


        public static int Populationsize { get => populationsize; set => populationsize = value; }
        public static int Total_aircraft { get => total_aircraft; set => total_aircraft = value; }
        public static int[] Size { get => size; set => size = value; }
        public static int[] Speed { get => speed; set => speed = value; }
        public static int[] Time { get => time; set => time = value; }
        public static int[] Start { get => start; set => start = value; }
        public static double Mutation_rate { get => mutation_rate; set => mutation_rate = value; }
        public static double Generation { get => generation; set => generation = value; }
    }
}
