﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Population
    {
        Config config;
        Field  fie;
        private Individual[] pop;
        public Population(Config conf,Field field)
        {
            this.fie = new Field(field);
            this.config = new Config(conf);
            this.Pop = new Individual[Config.Populationsize];
            for (int i = 0; i < Config.Populationsize; i++)
            {
                Pop[i] = new Individual(conf,field);
            }
        }

        internal Individual[] Pop { get => pop; set => pop = value; }
        public void crossOver()
        {
            int parent1, parent2;
            for (int child = Config.Populationsize / 2; child < Config.Populationsize; child= child+2)
            {
                do
                {
                    parent1 = this.selection();
                    parent2 = this.selection();
                }
                while (parent1 == parent2);

                this.crossOver(parent1, parent2, child, config);
            }
        }
        public void crossOver(int i,int j,int k,Config config)// generating twins
        {
            int random = config.rand.Next();
            double rand = config.rand.NextDouble();
            Individual child1 = new Individual(this.Pop[i]);
            Individual child2 = new Individual(this.Pop[j]);
            int sum_conflict = 0;
            int pa = 0;
            for (int a = 0; a < child1.Gene.Gene_list.Count; a++)
            {
                sum_conflict = sum_conflict + child1.Gene.Gene_list[a].Conflict;
                pa = pa + child1.Gene.Gene_list[a].Conflict;
            }
            for (int a = 0; a < +child2.Gene.Gene_list.Count; a++)
            {
                sum_conflict = sum_conflict + child2.Gene.Gene_list[a].Conflict;
            }//calculating the number of conflicts
            double pc = 0;
            pc = 1.0 - (((double)pa) / sum_conflict);//bag check
            for (int a = 0; a < this.Pop[k].Gene.Gene_list.Count; a++)
            {
                if (pc <= rand) //higher than 
                {
                    this.Pop[k].Gene.Gene_list[a] = child2.Gene.Gene_list[a];
                    this.Pop[k+1].Gene.Gene_list[a] = child1.Gene.Gene_list[a];
                }
                else
                {
                    this.Pop[k].Gene.Gene_list[a] = child1.Gene.Gene_list[a];
                    this.Pop[k + 1].Gene.Gene_list[a] = child2.Gene.Gene_list[a];
                }
               rand = config.rand.NextDouble();
            }
            this.Pop[k].ind_fcalc(this.fie);
            this.Pop[k+1].ind_fcalc(this.fie);
            //this.Pop[k + 1].Gene.Localsearch();
            this.Pop[k + 1].ind_fcalc(this.fie);
        }
        public int selection()//based on murata
        {
            int candidate1, candidate2, candidate3, candidate4;
            int elected;
            /* 2つの候補が等しくない物同士になるまで乱数をふる. */
            do
            {
                candidate1 = config.rand.Next(Config.Populationsize / 2);

                candidate2 = config.rand.Next(Config.Populationsize / 2);

                candidate3 = config.rand.Next(Config.Populationsize / 2);

                candidate4 = config.rand.Next(Config.Populationsize / 2);
            }
            while (candidate1 == candidate2 || candidate1 == candidate3 || candidate1 == candidate4 || candidate2 == candidate3 || candidate2 == candidate4 || candidate3 == candidate4);
                int win1;
                int win2;
            if ((this.Pop[candidate1].Infeasible == 1) && (this.Pop[candidate2].Infeasible == 0))
            {
                win1 = candidate2;
            }
            else if ((this.Pop[candidate1].Infeasible == 0) && (this.Pop[candidate2].Infeasible == 1))
            {
                win1 = candidate1;
            }
            else
            {
                if (this.Pop[candidate1].Fitness_ind >
                    this.Pop[candidate2].Fitness_ind)
                {
                    win1 = candidate2;
                }
                else if (this.Pop[candidate1].Fitness_ind <
                         this.Pop[candidate2].Fitness_ind)
                {
                    win1 = candidate1;
                }
                else
                {
                    win1 = (config.rand.Next() % 2 == 0) ? candidate1 : candidate2;
                }
            }
            if ((this.Pop[candidate3].Infeasible == 1) && (this.Pop[candidate4].Infeasible == 0))
            {
                win2 = candidate4;
            }
            else if ((this.Pop[candidate3].Infeasible == 0) && (this.Pop[candidate4].Infeasible == 1))
            {
                win2 = candidate3;
            }
            else
            {
                if (this.Pop[candidate3].Fitness_ind >
                   this.Pop[candidate4].Fitness_ind)
                {
                    win2 = candidate4;
                }
                else if (this.Pop[candidate3].Fitness_ind <
                         this.Pop[candidate4].Fitness_ind)
                {
                    win2 = candidate3;
                }
                else
                {
                    win2 = (config.rand.Next() % 2 == 0) ? candidate3 : candidate4;
                }
            }
            if ((this.Pop[win1].Infeasible == 1) && (this.Pop[win2].Infeasible == 0))
            {
                elected = candidate4;
            }
            else if ((this.Pop[win1].Infeasible == 0) && (this.Pop[win2].Infeasible == 1))
            {
                elected = win1;
            }
            else
            {
                if (this.Pop[win1].Fitness_ind >
                    this.Pop[win2].Fitness_ind)
                {
                    elected = win2;
                }
                else if (this.Pop[win1].Fitness_ind <
                         this.Pop[win2].Fitness_ind)
                {
                    elected = win1;
                }
                else
                {
                    elected = (config.rand.Next() % 2 == 0) ? win1 : win2;
                }
            }
  
            return elected;
        }
        public void Mutation()
        {
            for (int i = Config.Populationsize / 2; i < Config.Populationsize; i++)
            {
                this.Pop[i].Gene.Mutation();
                this.Pop[i].ind_fcalc(this.fie);
            }
        }
    }
}
